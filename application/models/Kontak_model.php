<?php
class Kontak_model extends CI_Model
{
    function add_inbox($name, $email, $phone, $subyek, $pesan)
    {
        $max = $this->db->query("SELECT MAX(INBOX_ID) as MAX FROM T_INBOX")->row()->MAX;
        $id = $max + 1;
        //$post['INBOX_ID'] = $INBOX_ID;
        // $id = $this->db->insert('T_INBOX', $post);  
        // return $id;


        $sql = $this->db->query("INSERT INTO T_INBOX (INBOX_ID,INBOX_NAME,INBOX_EMAIL,INBOX_PHONE,INBOX_TITLE,INBOX_MESSAGE,INBOX_STATUS,INBOX_RESPONSE) VALUES ('$id','$name','$email','$phone','$subyek','$pesan',0,'-')");
        return $sql;
    }

    function view_faq()
    {
        // $sql = $this->db->query("SELECT FAQ_CAT_ID,FAQ_CAT_NAME,FAQ_CAT_DESC FROM M_FAQ_CAT GROUP BY FAQ_CAT_ID,FAQ_CAT_NAME,FAQ_CAT_DESC");
        // return $sql;

        $this->db->select('M_FAQ_CAT.FAQ_CAT_ID,M_FAQ_CAT.FAQ_CAT_NAME,M_FAQ_CAT.FAQ_CAT_DESC');
        $this->db->join('M_FAQ_CAT', 'T_FAQ.FAQ_CAT_ID = M_FAQ_CAT.FAQ_CAT_ID');
        $this->db->from('T_FAQ');
        $this->db->group_by('M_FAQ_CAT.FAQ_CAT_ID,M_FAQ_CAT.FAQ_CAT_NAME,M_FAQ_CAT.FAQ_CAT_DESC');
        $this->db->where('T_FAQ.IS_ACTIVE', 1);
        $query = $this->db->get();
        return $query->result();
    }

    function detail_faq($textcategory)
    {
        // $sql = $this->db->query("SELECT * FROM T_FAQ WHERE FAQ_CAT_ID='$idcategory'");
        // return $sql;

        $this->db->select('T_FAQ.*');
        $this->db->join('M_FAQ_CAT', 'T_FAQ.FAQ_CAT_ID = M_FAQ_CAT.FAQ_CAT_ID');
        $this->db->from('T_FAQ');
        $this->db->where('M_FAQ_CAT.FAQ_CAT_NAME', $textcategory);
        $this->db->where('T_FAQ.IS_ACTIVE', 1);
        $query = $this->db->get();
        return $query->result();
    }
}
