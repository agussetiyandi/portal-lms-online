<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'portal';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*Portal*/

$route['portal-home'] = 'portal/index/1';
$route['portal-informasi'] = 'portal/index/2';
$route['portal-pengumuman'] = 'portal/index/3';
$route['portal-gallery'] = 'portal/index/4';
$route['portal-faq'] = 'portal/index/5';
$route['portal-kontak'] = 'kontak';
//$route['portal-menu1'] = 'portal/index/6';

$route['detail-home/(:any)'] = 'portal/data_detail/home/$1';
$route['detail-informasi/(:any)'] = 'portal/data_detail/informasi/$1';
$route['detail-pengumuman/(:any)'] = 'portal/data_detail/pengumuman/$1';
$route['detail-faq/(:any)'] = 'portal/data_detail/faq/$1';

/* //Portal*/
