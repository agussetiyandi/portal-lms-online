<div class="container mt-2">
	<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash');?>"></div>
	<div class="mb-5">
		<h4 class="wow fadeInUp"><b>Kontak</b></h4><br>
		<form class="form-horizontal cmxform"  id="frm_input" method="post" role="form" action="<?= base_url().'kontak/kirim'?>" enctype="multipart/form-data" >
			<div class="form-row">
				<div class="col-md-6">
					<div class="form-group">
						<input name="INBOX_NAME" id="name" placeholder="Nama lengkap anda*" type="text" class="form-control" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input name="INBOX_EMAIL" id="email" placeholder="Email anda*" type="email" class="form-control" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input name="INBOX_PHONE" id="nohp" placeholder="Nomor handphone anda*" type="number" class="form-control" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input name="INBOX_TITLE" id="subject" placeholder="Subject*" type="text" class="form-control" required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="row form-group">
						<div class="col-sm-12">
							<textarea name="INBOX_MESSAGE" id="isi" placeholder="Isi pesan*" class="form-control" required></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="row form-group">
						<div class="col-md-12">
							<!-- <button type="button" class="form-control" data-toggle="modal" data-target="#myModal">
							<button type="button" class="form-control" >
							    Kirim
							</button> -->
							<button style="width: 100%;" type="submit" class="btn btn-light waves-effect waves-light mr-1">Kirim</button>
						</div>
					</div>
				</div>
			</div>
		</form>
			<br>
			<div class="row">
				<br>
				<div class="col-md-4" style="text-align: center;">
					<span><i class="fa fa-map-marker fa-3x"></i></span>
					<p>Jl. Sudirman No.1, Tlk. Tering, Batam Kota, Kota Batam, Kepulauan Riau 29400</p>
				</div>
				<div class="col-md-4" style="text-align: center;">
					<span><i class="fa fa-envelope fa-3x"></i></span><p>lms-online@bpbatam.go.id</p>
				</div>
				<div class="col-md-4" style="text-align: center;">
					<span><i class="fa fa-phone fa-3x"></i></span><p><a style="color: black;" href="https://api.whatsapp.com/send?phone=6281364711807">081364711807</a> / <a style="color: black;" href="https://api.whatsapp.com/send?phone=6281364701797">081364701797</a></p>
				</div>
			</div>
	</div>
	<a href="https://api.whatsapp.com/send?phone=6281364701797" class="float" target="_blank">
		<i class="fa fa-whatsapp my-float"></i>
	</a>
</div>


<script src="<?= base_url().'assets/js/sweetalert2.all.min.js'?>"></script>
<script src="<?= base_url().'assets/js/popup.js'?>"></script>
