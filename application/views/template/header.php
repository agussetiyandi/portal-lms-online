<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="http://172.16.100.17:3000/logo.png">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/lc_lightbox.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/owl.carousel.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/owl.theme.default.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/hover.css'?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/animate.css'?>">
    <script src="<?= base_url().'assets/css/carousel.css'?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="<?= base_url().'assets/js/lc_lightbox.lite.min.js'?>"></script>
	<script src="<?= base_url().'assets/lib/AlloyFinger/alloy_finger.min.js'?>"></script>
	<script src="<?= base_url().'assets/js/owl.carousel.min.js'?>"></script>
	<script src="<?= base_url().'assets/js/vanilla-back-to-top.min.js'?>"></script>
	<script src="<?= base_url().'assets/js/wow.js'?>"></script>

	<style type="text/css">
		body{			
			font-family: 'Roboto', sans-serif;
			scroll-behavior: smooth;
		}
		a:hover{
			text-decoration: none;
		}
		a:active {
		    text-decoration:none;
		    color:#ffffff;
		    border:0px;
		    -moz-outline-style:none;
		}
		a:focus{
		   outline:none;
		   -moz-outline-style:none;
		}
		.fly {
		  opacity: 0;
		  transition: all 600ms ease-in-out;
		  transform: translateY(100px) scale(1.05) translate3d(0, 0, 0);
		}
		.show-block {
		  opacity: 1;
		  transform: translateY(0) scale(1) translate3d(0, 0, 0);
		}
		.float{
			position:fixed;
			width:60px;
			height:60px;
			bottom:90px;
			right:20px;
			background-color:#25d366;
			color:#FFF;
			border-radius:50px;
			text-align:center;
		    font-size:30px;
			box-shadow: 2px 2px 3px #999;
		    z-index:100;
		}
		.my-float{
			margin-top:16px;
		}
		h4{
			letter-spacing: 4px;
		}
		h5{
			letter-spacing: 1px;
			text-align: center;
			font-size: 25px;
		}
		nav{		
			transition: background-color 0.2s;
			-webkit-transition: background-color 0.2s;
		}
		.nav-link{
			text-transform: capitalize;
			font-size: 16px;
			letter-spacing: 0.2px;
		}
		.changeColor{
			background-color: #ffffff !important;
			box-shadow: 2px 1px 7px #999;
		}
		.thatHover:hover{
			opacity: 0.6;
      		color: black;
			transition: background-color 0.2s;
		}

	</style>

    <title>Portal LMS | BP Batam</title>
    <div id="root">
    	<div>
	    <nav class="navbar navbar-expand-md navbar-light bg-light fixed-top" style="background-color: #ffffff !important;">
	    	<div class="container">
				<a class="navbar-brand" href="<?= base_url();?>"><img src="http://172.16.100.17:3000/static/media/lmsol-logo.6b6f5065.png" class="card-img" style="width: 180px;"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse col-sm-6" aria-expanded="false" id="navbarNavAltMarkup">
					<ul class="ml-auto navbar-nav">
						<li class="nav-item"><b><a href="<?= base_url().'portal-informasi'?>" class="nav-link hvr-underline-from-center">Informasi</a></b>
						</li>&nbsp&nbsp
						<li class="nav-item"><b><a href="<?= base_url().'portal-pengumuman'?>" class="nav-link hvr-underline-from-center" ">Pengumuman</a></b>
						</li>&nbsp&nbsp
						<li class="nav-item"><b><a href="<?= base_url().'portal-gallery'?>" class="nav-link hvr-underline-from-center" ">Galeri</a></b>
						</li>&nbsp&nbsp
						<li class="nav-item"><b><a href="<?= base_url().'portal-faq'?>" class="nav-link hvr-underline-from-center" ">Faq</a></b></li>&nbsp
						<li class="nav-item"><b><a href="<?= base_url().'portal-kontak'?>" class="nav-link hvr-underline-from-center" ">Kontak</a></b>
						</li>&nbsp
						<!-- <li class="nav-item"><b><a href="<?= base_url().'portal-menu1'?>" class="nav-link hvr-underline-from-center" ">Menu1</a></b> -->
						</li>&nbsp&nbsp
						<li class="nav-item"><b><a href="http://172.16.100.17/v2/lms/" class="nav-link hvr-underline-from-center" ">Login</a></b>
						</li>
					</ul>
				</div>
		  	</div>
		</nav>
		</div><br><br><br>

		<div style="background-color: rgb(167, 239, 185);">
			<div>
				<?php if (count($text)!=0) {?>
				<marquee>
					<p style="margin-top: 10px;">
						<?php foreach ($text as $row_text) : ?>
						<?php if (!empty($row_text['INFO_TEXT'])) {
							echo $row_text['INFO_TEXT'];
						}echo '&emsp;&emsp;&emsp;&emsp;';
						endforeach; ?> 
					</p>
				</marquee>
				<?php }?>
			</div>
		</div>

  </head>
  <body>