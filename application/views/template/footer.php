	    <!-- Optional JavaScript -->
	    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	</body>

	<div style="background-color: rgb(29, 33, 36); height: 295px;">
		<div class="container mt-4"><br>
			<div class="row">
				<div class="col-sm-5">
					<img src="http://172.16.100.17:3000/static/media/lmsol-logo.6b6f5065.png" class="card-img" style="width: 180px;"><br><br>
					<p style="font-size: 14px; color: white;line-height: 25px;">
					Merupakan aplikasi pelayanan publik untuk masayarakat melakukan pengajuan permohonan perijinan yang ada di Kantor Pengelolaan Lahan BP Batam.
					</p>
					<span><i class="fa fa-facebook-square" style="color: white;"></i></span> |<span><i class="fa fa-instagram" style="color: white;"></i></span> |<span><i class="fa fa-twitter" style="color: white;"></i></span> |
				</div>
				<div class="col-sm-1"></div>
				<div class="col-sm-3"><br>
					<h4 style="color: white;font-weight: bold;letter-spacing: 0px;">Layanan</h4>
					<a href="<?=base_url('portal-kontak');?>" style="color: white; font-size: 14px;"> Contact</a><br>
					<a href="http://172.16.100.17" style="color: white; font-size: 14px;"> Login</a>
				</div>
				<div class="col-sm-3"><br>
					<h4 style="color: white;font-weight: bold;letter-spacing: 0px;">Kontak</h4>
					<p style="color: white; font-size: 14px;line-height: 25px;">
					Jl. Sudirman No.1, Tlk. Tering, Batam Kota, Kota Batam, Kepulauan Riau 29400 lms-online@bpbatam.go.id <a style="color: white;" href="https://api.whatsapp.com/send?phone=6281364711807">081364711807</a> / <a style="color: white;" href="https://api.whatsapp.com/send?phone=6281364701797">081364701797</a></p>
				</div>
			</div><hr>
			<p style="color: white; font-size: 14px; text-align: center;">Copyright © 2019, LMS - Land Management System BP Batam All Right Reserved</p>
		</div>
	</div>
</div>
<script>
	addBackToTop({
	  "diameter": 56,
	  "backgroundColor": "rgb(255, 82, 82)",
	  "textColor": "#fff",
	  scrollDuration: 1000,
	})
</script>
<script>
	wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
</script>
<script>     
 $(document).ready(function(){
      
      $(window).scroll(function(){

        if($(window).scrollTop()>100){
          $('nav').addClass('changeColor');
        }else{
          $('nav').removeClass('changeColor');
        }

      });

 });
</script>


</html>