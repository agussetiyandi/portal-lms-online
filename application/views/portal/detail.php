<div class="container mt-2">
	<div>
	<!-- home -->
	<?php if($name=='home') { ?>
		<?php foreach ($detail as $row) : ?>
			<div class="card card-body wow fadeInUp mt-2" style="border: none;">
			    <div class="row">
					<div class="col-sm-3">
						<img style="width:90%; height: 170px;" src="<?= $row['INFO_PHOTO'];?>" class="card-img-top">
					</div>
					<div class="col-sm-9">
						<div class="card-body">
							<div><p><?= $row['INFO_DESC'];?></p></div>
							<div><p class="card-text" style="font-weight: bold; font-size: 12px;"> | <?= substr($row['CREATED_DATE'],0,16);?></p></div><br>
							<?php if(str_word_count($row['INFO_DOCUMENT']) > 5) { ?>
								<div><p><a href="<?= $row['INFO_DOCUMENT'];?>" class="btn btn-primary waves-effect waves-light">Unduh lampiran</a></p></div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php } ?>
	<!-- endhome -->

	<!-- informasi -->
	<?php if($name=='informasi') { ?>
		<?php foreach ($detail as $row) : ?>
			<div class="card card-body wow fadeInUp mt-2" style="border: none;">
				<div class="card-title" style="font-size: 25px;font-weight: bold;text-align: center;">
					<?= $row['INFO_TITLE'];?><br><br>
				</div>
				<img src="<?= $row['INFO_PHOTO'];?>" class="img-fluid mx-auto d-block" style="width: 800px;">
				<div class="card-body">
					<div>
						<?= $row['INFO_DESC'];?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php } ?>
	<!-- endinformasi -->

	<!-- pengumuman -->
	<?php if($name=='pengumuman') { ?>
		<?php foreach ($detail as $row) : ?>
			<div class="card card-body wow fadeInUp mt-2" style="border: none;">
				<div class="card-title" style="font-size: 25px;font-weight: bold;text-align: center;">
					<?= $row['INFO_TITLE'];?><br><br>
				</div>
				<img src="<?= $row['INFO_PHOTO'];?>" class="img-fluid mx-auto d-block" style="width: 800px;">
				<div class="card-body">
					<div>
						<?= $row['INFO_DESC'];?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php } ?>
	<!-- endpengumuman -->

	<!-- faq -->
	<?php if($name=='faq') { ?>
		<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/style_list_faq.css'?>"">
		<div class="form-group">
	        <div class="input-group wow fadeInUp mt-4">
	            <input type="hidden" name="<?=$id;?>" id="id_kategori" value="<?=$id;?>">
	            <input style="font: bold;font-size: 18px;" type="text" name="search_text" id="search_text" placeholder="Search" class="form-control" />
	        </div>
	    </div>
	    <br />
	    <div id="result"></div>
	    <hr>
		<div style="clear:both"></div>
	<?php } ?>
	<!-- endfaq -->
	<a href="https://api.whatsapp.com/send?phone=6281364701797" class="float" target="_blank">
			<i class="fa fa-whatsapp my-float"></i>
		</a>
	</div>
</div>
</div>


<script>
$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  var id = $("#id_kategori").val();
  $.ajax({
   url:"<?php echo base_url(); ?>portal/dataCategory",
   method:"POST",
   data:{query:query,id:id},
   success:function(data){
    $('#result').html(data);
   }
  })
 }

 $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
});
</script>
