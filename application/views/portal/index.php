<div class="container">
	<div>
		<!-- home -->
		<?php if($id==1) { ?>
			<!-- landing -->
			<div>
				<div class="row">
					<?php foreach ($landing as $row_landing) : ?>
						<?php if($row_landing['IS_ACTIVE']==1) {?>
							<div class="col-md-6 wow slideInLeft" data-wow-duration="2s">
								<br><br><br><br><br><br><br><br><br>
								<h3 style="color: rgb(211, 158, 0);"><?= $row_landing['LANDING_TITLE'];?></h3><br>
								<?= $row_landing['LANDING_DESC'];?>
							</div>
							<div class="col-md-6 wow slideInRight" data-wow-duration="2s">
								<br><br>
								<img src="<?= $row_landing['LANDING_PHOTO'];?>" class="card-img">
							</div>
						<?php } ?>
					<?php endforeach; ?>
				</div>
				<br><hr><br>
			</div>
			<!-- endlanding -->
			<!-- tentang -->
			<div>
				<?php if(!empty($tentang)) {?>
					  <?php foreach ($tentang as $row_tentang) : ?>
						<h5 class="wow fadeInUp" style="text-transform: capitalize;"><b><?= $row_tentang['INFO_TITLE'];?></b></h5><br>
						<p><?= $row_tentang['INFO_DESC'];?></p>
					  <?php endforeach; ?>
				<br><hr><br>
				<?php }?>
			</div>
			<!-- endtentang -->
			<!-- layanan -->
			<main >
				<h5 class="wow fadeInUp"><b>Pelayanan LMS Online</b></h5><br><br>
				<div class="row">
					 <div class="owl-carousel owl-theme">
					  <?php foreach ($layanan as $row_layanan) : ?>
				        <div class="item wow fadeInLeft thatHover" data-wow-duration="2s">
				        	<a href="<?= base_url().'detail-home/'.$row_layanan['PERMALINK'];?>">
				        		<img style="width:150px;height:150px;display: block;margin-left: auto;margin-right: auto;box-shadow: 0 0 0 #999;" src="<?= $row_layanan['INFO_PHOTO_THUMB'];?>" class="card-img"><br>
				        		<p style="color: rgb(25, 24, 24); font-size: 15px; text-align: center;"><?= $row_layanan['INFO_TITLE'];?></p>
							</a>
				        </div>
					  <?php endforeach; ?>
				    </div>
				</div>
			   <br><hr><br>
			</main>
			<!-- endlayanan -->
			<!-- gallery-foto -->
			<main >
				<h5 class="wow fadeInUp"><b>Galeri Foto</b></h5><br><br>
				<div class="row">
					 <div class="owl-carousel owl-theme">
					  <?php foreach ($foto as $row_foto) : ?>
				        <div class="item wow fadeInRight thatHover" data-wow-duration="2s">
				        	<a class="mybox" href="<?= $row_foto['INFO_PHOTO'];?>" title="<?= $row_foto['INFO_TITLE'];?>">
								<img style="width: 260px; height: 200px;display: block;margin-left: auto;margin-right: auto;box-shadow: 5px 5px 7px #999;" src="<?= $row_foto['INFO_PHOTO_THUMB'];?>">		<br>
								<p style="color: rgb(25, 24, 24);text-align: center; font-size: 15px;"><?= $row_foto['INFO_TITLE'];?></p>					
							</a>
				        </div>
					  <?php endforeach; ?>
				    </div>				
				</div>
			   <br><hr><br>
			</main>
			<!-- endgallery-foto -->

			<!-- gallery-video -->
			<main >
				<h5 class="wow fadeInUp"><b>Galeri Video</b></h5><br><br>
				<div class="row">
					<div class="owl-carousel owl-theme mb-4">
					  <?php foreach ($video as $row_video) : ?>
				        <div class="item wow fadeInLeft thatHover" data-wow-duration="2s">
							<iframe width="262" height="200" src="<?= $row_video['YOUTUBE_CODE'];?>" allowfullscreen></iframe>
							<p style="color: rgb(25, 24, 24); font-size: 14px; text-align: center;"><?= $row_video['INFO_TITLE'];?></p>
				        </div>
					  <?php endforeach; ?>
				    </div>
				</div>
			</main>
			<!-- endgallery-video -->
		<?php } ?>
		<!-- endhome -->

		<!-- informasi -->
		<?php if($id==2) { ?>
			<main>
				<h4 class="wow fadeInUp mt-3"><b>Informasi</b></h4><br>
				<div class="mb-5">
				<?php foreach ($informasi as $row) : ?>
			    <div class="card card-body" style="border: none;border-top: 1px solid #E5E5E5;border-bottom: 1px solid #E5E5E5;">
				    <div class="row">
						<div class="col-sm-3">
							<img style="width:100%; height: 170px;" src="<?= $row['INFO_PHOTO_THUMB'];?>" class="img-fluid">
						</div>
						<div class="col-sm-9">
							<div class="card-body">
								<div class="card-title"><b><?= $row['INFO_TITLE'];?></b></div>
								<div><p><?= substr($row['INFO_DESC'],0,239);?></p></div>
								<p class="card-text" style="font-weight: bold; font-size: 12px;"> | <?= substr($row['CREATED_DATE'],0,16);?></p>
								<a href="<?= base_url().'detail-informasi/'.$row['PERMALINK'];?>"><p>Baca ...</p></a>
								<?php if(str_word_count($row['INFO_DOCUMENT']) > 5) { ?>
									<a href="<?= $row['INFO_DOCUMENT'];?>" class="btn btn-primary waves-effect waves-light">Unduh lampiran</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				</div>
			</main>
		<?php } ?>
		<!-- endinformasi -->		

		<!-- pengumuman -->
		<?php if($id==3) { ?>
			<main>
				<h4 class="wow fadeInUp mt-3"><b>Pengumuman</b></h4><br>	
				<div class="mb-5">
				<?php foreach ($pengumuman as $row) : ?>
			    <div class="card card-body" style="border: none;border-top: 1px solid #E5E5E5;border-bottom: 1px solid #E5E5E5;">
				    <div class="row">
						<div class="col-sm-3">
							<img style="width:100%; height: 170px;" src="<?= $row['INFO_PHOTO'];?>" class="card-img-top">
						</div>
						<div class="col-sm-9">
							<div class="card-body">
								<div class="card-title"><b><?= $row['INFO_TITLE'];?></b></div>
								<div><p><?= substr($row['INFO_DESC'],0,239);?></p></div>
								<p class="card-text" style="font-weight: bold; font-size: 12px;"> | <?= substr($row['CREATED_DATE'],0,16);?></p>
								<a href="<?= base_url().'detail-pengumuman/'.$row['PERMALINK'];?>"><p>Baca ...</p></a>
								<?php if(str_word_count($row['INFO_DOCUMENT']) > 5) { ?>
									<a href="<?= $row['INFO_DOCUMENT'];?>" class="btn btn-primary waves-effect waves-light">Unduh lampiran</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				</div>
			</main>
		<?php } ?>
		<!-- endpengumuman -->	

		<!-- gallery -->
		<?php if($id==4) { ?>
			<main >
				<h4 class="wow fadeInUp mt-3"><b>Galeri Foto</b></h4><br>
				<div class="row">
					 <div class="owl-carousel owl-theme">
					  <?php foreach ($foto as $row_foto) : ?>
				        <div class="item-image thatHover">
				        	<a class="mybox" href="<?= $row_foto['INFO_PHOTO'];?>" title="<?= $row_foto['INFO_TITLE'];?>">
								<img style="width: 260px; height: 200px;display: block;margin-left: auto;margin-right: auto;box-shadow: 5px 5px 7px #999;" src="<?= $row_foto['INFO_PHOTO_THUMB'];?>">		<br>					
								<p style="color: rgb(25, 24, 24);text-align: center; font-size: 14px;"><?= $row_foto['INFO_TITLE'];?></p>
							</a>
				        </div>
					  <?php endforeach; ?>
				    </div>				
				</div>
				<br><hr><br>
			</main>
			<main >
				<h4 class="wow fadeInUp"><b>Galeri Video</b></h4><br>
				<div class="row">
					<div class="owl-carousel owl-theme mb-4">
					  <?php foreach ($video as $row_video) : ?>
				        <div class="item thatHover">
							<iframe width="262" height="200" src="<?= $row_video['YOUTUBE_CODE'];?>" allowfullscreen></iframe>
							<p style="color: rgb(25, 24, 24); font-size: 14px; text-align: center;"><?= $row_video['INFO_TITLE'];?></p>
				        </div>
					  <?php endforeach; ?>
				    </div>
				</div>
			</main>
		<?php } ?>
		<!-- endgallery -->

		<!-- faq -->
		<?php if($id==5) { ?>
			<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/style_faq.css'?>">
			<main >
				<h4 class="wow fadeInUp mt-3"><b>Category</b></h4><br>
	            <div class="row mb-5">
	                <?php foreach ($category as $row_category) : ?>
	                <div class="column mb-5">
	                    <a class="card-link" href=" <?= base_url() . 'detail-faq/' . $row_category->FAQ_CAT_NAME; ?>">
	                        <div class="card rounded">
	                            <p></p>
	                            <b><?= $row_category->FAQ_CAT_NAME; ?></b>
	                            <p><?= $row_category->FAQ_CAT_DESC; ?></p>
	                        </div>
	                    </a>
	                </div>
	                <?php endforeach; ?>
	            </div>
			</main>
		<?php } ?>
		<!-- endfaq -->	


		<!-- menu1 -->
		<?php if($id==6) { ?>
			<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/style_faq.css'?>">
			<main >
				<h4 class="wow fadeInUp mt-3"><b></b></h4><br>
	            <div class="row mb-5">
					<div class="col-md-2"></div>
					<div class="col-md-7 mt-4 mb-5 ml-5">
						<h2 style="text-align: center;"><b>Butuh Bantuan ?</b></h2>
						<form action="" class=" mt-5 mb-5">
							<div class="input-group mb-3 input-group-lg">
							  <input type="text" class="form-control" placeholder="Cari disini..">
							  <div class="input-group-append">
							    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
							  </div>
							</div>
						</form>
					</div>
					<div class="col-md-6"></div>
	            </div>
			</main>
		<?php } ?>
		<!-- endmenu1 -->	
		<a href="https://api.whatsapp.com/send?phone=6281364701797" class="float" target="_blank">
			<i class="fa fa-whatsapp my-float"></i>
		</a>
	</div>
</div>

<script>
$('.owl-carousel').owlCarousel({
    loop:false,
    margin:10,
    responsiveClass:true,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        500:{
            items:2
        },
        1000:{
            items:4
        }
    }
})
</script>
<script type="text/javascript">
	lc_lightbox('.mybox', {
		wrap_class: 'lcl_fade_oc',
		gallery: true
	})
</script>