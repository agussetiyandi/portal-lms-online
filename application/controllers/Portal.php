<?php 

class Portal extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('kontak_model');
		$this->load->helper('date');
		$this->load->library('session');

		$this->API_L = "http://172.16.100.17/portal_services/";
		$this->API = "http://172.16.100.17/portal_services/data";
		$this->API_ROW = "http://172.16.100.17/portal_services/data_read";
	}

	public function index($id='1')
	{
		$data_text = json_decode(file_get_contents($this->API.'/runningtext'), true);
		$running['text'] = $data_text["results"];

		if($id==1){
			$data_landing = json_decode(file_get_contents($this->API_L.'/landing'), true);
			$data_tentang = json_decode(file_get_contents($this->API.'/tentang-lms'), true);
			$data_layanan = json_decode(file_get_contents($this->API.'/layanan'), true);
			$data_gallery_foto = json_decode(file_get_contents($this->API.'/gallery-foto'), true);
			$data_gallery_video = json_decode(file_get_contents($this->API.'/gallery-video'), true);
			$data = array(
				'landing' => $data_landing["results"],
				'tentang' => $data_tentang["results"],
				'layanan' => $data_layanan["results"],
				'foto' => $data_gallery_foto["results"],
				'video' => $data_gallery_video["results"],		
				'id' => $id	
			);
		}elseif($id==2){
			$data_informasi = json_decode(file_get_contents($this->API.'/information'), true);
			$data = array(
				'informasi' => $data_informasi["results"],		
				'id' => $id		
			);
		}elseif($id==3){
			$data_pengumuman = json_decode(file_get_contents($this->API.'/pengumuman'), true);
			$data = array(
				'pengumuman' => $data_pengumuman["results"],
				'id' => $id	
			);
		}elseif($id==4){
			$data_foto = json_decode(file_get_contents($this->API.'/gallery-foto'), true);
			$data_video = json_decode(file_get_contents($this->API.'/gallery-video'), true);
			$data = array(
				'foto' => $data_foto["results"],
				'video' => $data_video["results"],
				'id' => $id	
			);
		}elseif($id==5){
			$data['category'] = $this->kontak_model->view_faq();
			$data['id'] = $id;
		}elseif($id==6){
			$data['id'] = $id;
		}else{
			$this->load->view('index.html');die();			
		}

		$this->load->view('template/header',$running);
		$this->load->view('portal/index',$data);
		$this->load->view('template/footer');
	}

	public function data_detail($name='',$text='')
	{
		$data_text = json_decode(file_get_contents($this->API.'/runningtext'), true);
		$running['text'] = $data_text["results"];

		if($name=='home'){
			$data_detail_layanan = json_decode(file_get_contents($this->API_ROW.'/'.$text), true);
			$data = array(
				'detail' => $data_detail_layanan["results"],
				'name' => $name	
			);
		}elseif($name=='informasi'){
			$data_detail_informasi = json_decode(file_get_contents($this->API_ROW.'/'.$text), true);
			$data = array(
				'detail' => $data_detail_informasi["results"],
				'name' => $name	
			);
		}elseif($name=='pengumuman'){
			$data_detail_pengumuman = json_decode(file_get_contents($this->API_ROW.'/'.$text), true);
			$data = array(
				'detail' => $data_detail_pengumuman["results"],
				'name' => $name	
			);
		}elseif($name=='faq'){
			$data = array(
				'id' => $text,
				'name' => $name	
			);
		}else{
			redirect(base_url());die();
		}

		$this->load->view('template/header',$running);
		$this->load->view('portal/detail',$data);
		$this->load->view('template/footer');
	}

	public function dataCategory()
	{
		$id = str_replace('%20' , ' ', $this->input->post('id'));
		$data = $this->kontak_model->detail_faq($id);
		$x=1;$y=0;$z=0;
		$output = '';

		if ($input = $this->input->post('query')) {
			echo '<div id="accordion" class="mb-5">';

			foreach ($data as $item) {
			    if ($data = preg_match("/.$input./i",$item->FAQ_ASK_DESC->load()) or preg_match("/.$input./i",$item->FAQ_ANSWER_DESC->load())) {
			        echo '
						<div class="card rounded">
					    <div class="card-header" style="background-color: white;cursor: pointer;" data-toggle="collapse" href="#collapse'.$y.'">
					      <b>
					      	<a class="card-link" style="color: #212529;">
					        '.$item->FAQ_ASK_DESC->load().'
					      	</a>
					      </b>
					    </div>
					    <div id="collapse'.$z.'" class="collapse" data-parent="#accordion">
					      <div class="card-body">
					        '.$item->FAQ_ANSWER_DESC->load().'
					      </div>
					    </div>
					</div><br>';
			    }$y++;$z++;
			    
			}echo '</div>'; exit();
		}

		sort($data);
		$output .= '
		<div id="accordion" class="mb-5">
		';
		if(count($data) > 0)
		{
			if(is_array($data) || is_object($data))
			{
				foreach($data as $row_faq)
				{
				  $output .= '
					<div class="card rounded">
					    <div class="card-header" style="background-color: white;cursor: pointer;" data-toggle="collapse" href="#collapse'.$x.'">
					      <b>
					      	<a class="card-link" style="color: #212529;">
					        '.$row_faq->FAQ_ASK_DESC->load().'
					      	</a>
					      </b>
					    </div>
					    <div id="collapse'.$x.'" class="collapse" data-parent="#accordion">
					      <div class="card-body">
					        '.$row_faq->FAQ_ANSWER_DESC->load().'
					      </div>
					    </div>
					</div><br>
				 ';$x++;
				}
			}
		}else{
		 $output .= '<div class="card mb-5">
			    <div class="card-header">
			      <b><a style="color: #212529;" class="card-link" data-toggle="collapse">
			        No Data Found
			      </a></b>
			    </div>
			</div><br>';
		}
		 $output .= '</div><br>';
		 echo $output;
	}
}