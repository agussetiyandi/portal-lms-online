<?php 

class Kontak extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model(array('kontak_model'));
		$this->load->helper('date');
		$this->load->library('session');

		$this->API = "http://172.16.100.17/portal_services/data";
	}

	public function index()
	{

		$data_text = json_decode(file_get_contents($this->API.'/runningtext'), true);
		$running['text'] = $data_text["results"];
		$this->load->view('template/header',$running);
		$this->load->view('kontak/index');
		$this->load->view('template/footer');
		

		//  $this->load->library('session');
		// $post = $this->input->post('data');
		// if (empty($post)) {		
		// 	$data_text = json_decode(file_get_contents($this->API.'/runningtext'), true);
		// 	$running['text'] = $data_text["results"];
		// 	$this->load->view('template/header',$running);
		// 	$this->load->view('kontak/index');
		// 	$this->load->view('template/footer');
		// } 
		// else {											
		// 	//$post['CREATED_BY'] = '';
		// 	//$post['CREATED_DATE'] = $this->oracle_date('timestamp');
		// 	//print_r($post);exit();
		// 	$this->kontak_model->add_inbox($post);	
		// 	$this->session->set_flashdata('flash', 'terkirim');
		// 	redirect('kontak');
		// }
	}
	public function kirim()
	{

		$name = $this->input->post("INBOX_NAME");
		$email = $this->input->post("INBOX_EMAIL");
		$phone = $this->input->post("INBOX_PHONE");
		$subyek = $this->input->post("INBOX_TITLE");
		$pesan = $this->input->post("INBOX_MESSAGE");
		$this->kontak_model->add_inbox($name,$email,$phone,$subyek,$pesan);	
		$this->session->set_flashdata('flash', 'terkirim');
		redirect('kontak');
	}
}