<?php
class MY_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();            
    }

    public function oracle_date($timestamp=''){
        $this->load->helper('date');
        if($timestamp=='date'){
            $datestring = '%d-%M-%Y';
        }
        else
        {
            //  $datestring = '%d-%M-%Y %h.%i.%s %a';
            $datestring = '%d-%M-%Y %h:%i:%s %a';
        }
        
        $time = time();
        $timestamp = strtoupper(mdate($datestring, $time));
        return $timestamp;
    }
}
?>